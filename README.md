PHP - gsGlass
----------
This repository demonstrates an out-of-stock product request feature for a specialized glassware store.
It provides a potential customer with the ability to submit a request for notification when one or more products of their choosing are available for purchase.

### Key demonstrations:
- Dependency injection
- Input validation
- Object oriented development
- Separation of concerns

### Technologies used:
- jQuery
- PHP 7.1+
- SQLite

### Limitations and notes:
- Assumption made that piece request should not be a single record (with 1+ relational records) as the business
  may more frequently wish to notify the customer of _any_ in-stock request pieces rather than full request sets.
- The scope of this demonstration does not take into account the need for negotiation of DB transaction state.
  This would typically be recommended when performing more than one DB insert or record update.
- Personal preference would be to implement a custom query-builder class rather than use raw SQL. This is important
  when facilitating changes to DB architecture or when using method logic flow to implement query conditions such as
  joins or where conditions.
- In place of string literals, e.g. the SQLite execute method, I would prefer to use traits or constants for
  scalability. Those same declarations would prove beneficial in the QueryBuilder logic described above.
- The views used in this demonstration are limited by design. A practical approach would offer more modularity
  to reduce repetition and aid scalability.
- Personal preference would be for PHP-based input validation to be performed from a dedicated input validation
  class using a field definitions file. This reduces duplication of common evaluations and centralizes
  definition logic.
- In a proper product implementation I would _never_ recommend committing files such as configuration files that
  may contain sensitive information.

### Troubleshooting:
- Depending on how/where you extract this demonstration the web server may not have permissions to execute properly.
  If you are encountering unexpected 403 errors, or the database does not appear to be appropriately performing inserts
  try the following (assuming running in a unix environment):
  
  **NOTE 1**: These permissions are _not_ recommended for production use. This is for localized execution only. For a
          production environment it would be recommended that the webserver user be isolated from root/authenticating
          users and provided exclusive permissions to the web server directories and processes.
          
  **NOTE 2**: {repo_directory} represents the directory this repo was cloned/extracted to.
  - `chmod 755 -R {repo_directory}` 755 indicates recursively full permissions to owner but only
    read/execute to group/others
  - `chmod 777 {repo_directory}/api/databases` 777 indicates full permissions to all on this path. This is needed
    as the database directory (and subsequently SQLite file) require edit files within this directory.
  - `chmod 766 {repo_directory}/api/databases/gsglass.sqlite` 766 indicates full permissions to owner but only
    read/write to group/others as these permissions are required to make changes to the SQLite DB file.
  