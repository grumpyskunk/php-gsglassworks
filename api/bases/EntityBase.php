<?php

namespace gsGlass\api\bases;

use gsGlass\api\services\DatabaseService;

class EntityBase {

  /** @var DatabaseService */
  protected $database_service;
  /** @var array Entities permitted user input based write-to fields */
  protected $permitted_fields = [];

  /**
   * BaseEntity constructor.
   *
   * @param DatabaseService $database_service
   */
  public function __construct(DatabaseService $database_service) {
    $this->database_service = $database_service;
  }

  /**
   * Builds an insert query string
   * This is hack-y... note mention of using a QueryBuilder
   *
   * @param string $table
   * @param array  $params
   *
   * @return string
   */
  protected function buildInsertQuery(string $table, array $params): string {
    $sql = 'insert into ' . $table . ' (' . implode(', ', array_keys($params)) . ') values (';

    $two_plus = false;
    foreach ($params as $key => $value) {
      if ($two_plus) {
        $sql .= ', ';
      }

      $sql      .= ':' . $key;
      $two_plus = true;
    }

    $sql .= ')';

    return $sql;
  }

  /**
   * Creates parameter array for insert/update record requests
   *
   * @param array $input
   *
   * @return array
   */
  protected function buildParameters(array $input): array {
    $parameters = [];

    foreach ($this->permitted_fields as $field) {
      // If this field key exists within the input, use it
      if (array_key_exists($field, $input)) {
        $parameters[$field] = $input[$field];
      }
    }

    return $parameters;
  }

  /**
   * Performs execution of given database query
   *
   * @param string $sql
   * @param array  $parameters
   *
   * @return array|int|string
   * @throws \Exception
   */
  protected function execute(string $sql, array $parameters = []) {
    return $this->database_service->execute($sql, $parameters);
  }

  /**
   * Performs execution of database query with expectation of single record return
   *
   * @param string $sql
   * @param array  $parameters
   *
   * @return array|mixed|string
   * @throws \Exception
   */
  protected function first(string $sql, array $parameters = []) {
    $result = $this->execute($sql, $parameters);

    return $result[0] ?? [];
  }

}
