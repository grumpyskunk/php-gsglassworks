<?php

namespace gsGlass\api\entities;

use gsGlass\api\bases\EntityBase;
use http\Exception;

class Customer extends EntityBase {

  protected $permitted_fields = [
    'name',
    'email',
    'phone',
  ];

  /**
   * Creates a new customer record
   *
   * @param array $input
   *
   * @return string
   * @throws \Exception
   */
  public function create(array $input) {
    $params = $this->buildParameters($input);

    $sql = $this->buildInsertQuery('customers', $params);

    return $this->execute($sql, $params);
  }

  /**
   * Retrieves an existing customer by email address
   *
   * @param string $email
   *
   * @return array
   * @throws \Exception
   */
  public function getByEmail(string $email): array {
    $sql = 'select * from customers where email = :email limit 1';

    return $this->first($sql, ['email' => $email]);
  }

  /**
   * Updates an existing customer record
   *
   * @param int   $record_id
   * @param array $input
   *
   * @return void
   * @throws \Exception
   */
  public function update(int $record_id, array $input): void {
    $params = $this->buildParameters($input);

    $sql = 'update customers set ';

    $two_plus = false;
    foreach (array_keys($params) as $key) {
      if ($two_plus) {
        $sql .= ', ';
      }

      $sql      .= $key . ' = :' . $key;
      $two_plus = true;
    }

    $sql .= ' where id = :id';

    // Add record ID for where condition
    $params['id'] = $record_id;

    $result = $this->execute($sql, $params);

    if ($result !== 1) {
      throw new \Exception('Customer record not updated: ' . $record_id);
    }
  }

}