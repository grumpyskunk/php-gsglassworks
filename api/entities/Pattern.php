<?php

namespace gsGlass\api\entities;

use gsGlass\api\bases\EntityBase;

class Pattern extends EntityBase {

  /**
   * Retrieve all pattern records
   *
   * @return array
   * @throws \Exception
   */
  public function getAll(): array {
    $sql = 'select * from patterns';

    return $this->execute($sql);
  }

}
