<?php

namespace gsGlass\api\entities;

use gsGlass\api\bases\EntityBase;

class PieceRequest extends EntityBase {

  protected $permitted_fields = [
    'customer_id',
    'piece_type_id',
    'pattern_id',
    'quantity',
  ];

  /**
   * Creates a new piece request record
   *
   * @param array $input
   *
   * @return array|int|string
   * @throws \Exception
   */
  public function create(array $input) {
    $params = $this->buildParameters($input);

    $sql = $this->buildInsertQuery('piece_requests', $params);

    return $this->execute($sql, $params);
  }

}