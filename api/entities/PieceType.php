<?php

namespace gsGlass\api\entities;

use gsGlass\api\bases\EntityBase;

class PieceType extends EntityBase {

  /**
   * Retrieve all piece type records
   *
   * @return array
   * @throws \Exception
   */
  public function getAll(): array {
    $sql = 'select * from piece_types';

    return $this->execute($sql);
  }

}
