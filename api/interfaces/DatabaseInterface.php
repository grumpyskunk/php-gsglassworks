<?php

namespace gsGlass\api\interfaces;

interface DatabaseInterface {

  /** Database connect method */
  public function connect();

  /** Database disconnect method */
  public function disconnect(): void;

  /** Database query execute method */
  public function execute(string $sql, array $parameters = []);

  /** Last record insert ID method */
  public function lastInsertId();

  /** Query prepare method */
  public function prepare(string $sql);

}