<?php

namespace gsGlass\api\resources;

use Exception;
use ReflectionClass;

class DependencyInjector {

  private $singletons = [];
  private $objects    = [];

  /**
   * Build object and dependencies
   *
   * @param string $class Class to build
   *
   * @return mixed
   * @throws Exception
   */
  public function buildObject(string $class) {
    // If we have already built this object...
    if (($this->objects[$class] ?? false) && array_key_exists($class, $this->singletons) !== true) {
      return $this->objects[$class];
    }

    // Retrieve a reflection of the class to build
    $reflection = new ReflectionClass($class);

    // Retrieve class constructor
    $constructor = $reflection->getConstructor();
    // If no constructor this class is dependency free (jealous)
    if ($constructor === null) {
      $object                = $reflection->newInstance();
      $this->objects[$class] = $object;

      return $object;
    }

    // Otherwise let us determine the dependencies needed here
    $parameters = $constructor->getParameters();
    $builders   = [];
    // Build parameter objects from constructor
    foreach ($parameters as $parameter) {
      // Build object and add to dependency builders
      $object     = $this->buildObject($parameter->getType());
      $builders[] = $object;

      // Add to objects
      $this->objects[get_class($object)] = $object;
    }

    // Define our new object from builders
    $object                = $reflection->newInstanceArgs($builders);
    $this->objects[$class] = $object;

    return $object;
  }

}
