<?php

namespace gsGlass\api\resources;

use gsGlass\api\interfaces\DatabaseInterface;

class Sqlite implements DatabaseInterface {

  /** @var \SQLite3 */
  private $connection;
  /** @var string */
  private $db_path;
  /** @var bool */
  private $in_transaction = false;

  /**
   * Sqlite constructor.
   */
  public function __construct() {
    $this->db_path = APP_PATH . '/api/' . ENV['database']['location'];
  }

  /**
   * Initiates SQLite database connection
   *
   * @return \SQLite3
   * @throws \Exception
   */
  public function connect(): \SQLite3 {
    if ($this->connection === null) {
      $this->connection = new \SQLite3($this->db_path);
    }

    return $this->connection;
  }

  /**
   * Disconnects database connection
   *
   * @return void
   */
  public function disconnect(): void {
    $this->connection->close();
  }

  /**
   * Performs execution of given database query
   *
   * @param string $sql
   * @param array  $parameters
   *
   * @return null|array|int|string
   * @throws \Exception
   */
  public function execute(string $sql, array $parameters = []) {
    // Lower our SQL
    $sql = strtolower($sql);

    $statement = $this->prepare($sql);
    foreach ($parameters as $key => $value) {
      $statement->bindValue(':' . $key, $value);
    }

    $result = $statement->execute();
    if ($result !== false) {
      if (strpos($sql, 'insert') === 0) {
        // If we are performing an insert we will return the record ID...

        return $this->lastInsertId();
      } else if (strpos($sql, 'update') === 0 || strpos($sql, 'delete') === 0) {
        // If we are performing an update or delete we will return the number of rows affected...

        return $this->connection->changes();
      } else {
        // Otherwise return the rows from a SELECT

        $return = [];
        while ($record = $result->fetchArray(SQLITE3_ASSOC)) {
          $return[] = $record;
        }

        return $return;
      }
    }

    return null;
  }

  /**
   * Retrieves last insert ID
   *
   * @return string
   */
  public function lastInsertId(): string {
    return $this->connection->lastInsertRowID();
  }

  /**
   * Prepares an SQL statement
   *
   * @param string $sql
   *
   * @return \SQLite3Stmt
   * @throws \Exception
   */
  public function prepare(string $sql): \SQLite3Stmt {
    $statement = $this->connection->prepare($sql);

    if ($statement === false) {
      $this->disconnect();

      throw new \Exception('Unable to prepare SQL statement: ' . $sql);
    }

    return $statement;
  }

  /**
   * Redundant disconnect is redundant
   */
  public function __destruct() {
    $this->disconnect();
  }

}