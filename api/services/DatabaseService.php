<?php

namespace gsGlass\api\services;

use gsGlass\api\interfaces\DatabaseInterface;
use gsGlass\api\resources\Sqlite;

class DatabaseService {

  /** @var DatabaseInterface */
  private $database;

  /**
   * DatabaseService constructor.
   *
   * @param Sqlite $database
   *
   * @throws \Exception
   */
  public function __construct(Sqlite $database) {
    $this->database = $database;

    $this->database->connect();
  }

  /**
   * Performs execution of given database query
   *
   * @param string $sql
   * @param array  $parameters
   *
   * @return array|int|string
   * @throws \Exception
   */
  public function execute(string $sql, array $parameters = []) {
    return $this->database->execute($sql, $parameters);
  }

}