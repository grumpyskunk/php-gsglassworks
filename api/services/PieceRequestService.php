<?php

namespace gsGlass\api\services;

use gsGlass\api\entities\Customer;
use gsGlass\api\entities\Pattern;
use gsGlass\api\entities\PieceRequest;
use gsGlass\api\entities\PieceType;

class PieceRequestService {

  /** @var Customer */
  private $customer;
  /** @var Pattern */
  private $pattern;
  /** @var PieceRequest */
  private $piece_request;
  /** @var PieceType */
  private $piece_type;

  /**
   * PieceRequestService constructor.
   *
   * @param Customer     $customer
   * @param Pattern      $pattern
   * @param PieceRequest $piece_request
   * @param PieceType    $piece_type
   */
  public function __construct(
    Customer $customer,
    Pattern $pattern,
    PieceRequest $piece_request,
    PieceType $piece_type
  ) {
    $this->customer      = $customer;
    $this->pattern       = $pattern;
    $this->piece_request = $piece_request;
    $this->piece_type    = $piece_type;
  }

  /**
   * Creates a new customer record
   *
   * @param array $input
   *
   * @return mixed
   * @throws \Exception
   */
  public function createCustomer(array $input) {
    return $this->customer->create($input);
  }

  /**
   * Creates a new piece request record
   *
   * @param array $input
   *
   * @return array|int|string
   * @throws \Exception
   */
  public function createPieceRequest(array $input) {
    return $this->piece_request->create($input);
  }

  /**
   * Retrieves all pattern records
   *
   * @return array
   * @throws \Exception
   */
  public function getAllPatterns(): array {
    return $this->pattern->getAll();
  }

  /**
   * Retrieves all piece type records
   *
   * @return array
   * @throws \Exception
   */
  public function getAllPieceTypes(): array {
    return $this->piece_type->getAll();
  }

  /**
   * Retrieves an existing customer by email address
   *
   * @param string $email
   *
   * @return array
   * @throws \Exception
   */
  public function getCustomerFromEmail(string $email): array {
    return $this->customer->getByEmail($email);
  }

  /**
   * Places a piece request order
   *
   * @param $request_input
   *
   * @throws \Exception
   */
  public function placeRequest(array $request_input): void {
    // Check if an existing customer exists by the given email
    $customer = $this->getCustomerFromEmail($request_input['email']);

    // If none exists create one...
    if (empty($customer)) {
      $customer_id = $this->createCustomer($request_input);
    } else {
      // Otherwise retrieve the known customer ID and update their name/phone
      $customer_id = $customer['id'];
      $this->updateCustomer($customer_id, $request_input);
    }

    // For each request type submitted...
    foreach ($request_input['piece_type_ids'] as $index => $type) {
      // Build and create a new request record
      $request = [
        'customer_id'   => $customer_id,
        'piece_type_id' => $request_input['piece_type_ids'][$index],
        'pattern_id'    => $request_input['pattern_ids'][$index],
        'quantity'      => $request_input['quantities'][$index],
      ];
      $this->createPieceRequest($request);
    }
  }

  /**
   * Updates an existing customer record
   *
   * @param int   $record_id
   * @param array $input
   *
   * @throws \Exception
   */
  public function updateCustomer(int $record_id, array $input): void {
    $this->customer->update($record_id, $input);
  }

  /**
   * Perform input validation
   *
   * @param array $input
   *
   * @return bool
   */
  public function validateRequest(array $input): bool {
    // If name empty or unset
    if (empty($input['name'] ?? [])) {
      return false;
    }

    // If email empty or unset
    if (empty($input['email'] ?? [])) {
      return false;
    }

    /**
     * Count request group arrays
     */
    $piece_counts    = count($input['piece_type_ids'] ?? []);
    $pattern_counts  = count($input['pattern_ids'] ?? []);
    $quantity_counts = count($input['quantities'] ?? []);

    // If any of our request group properties are zero
    if ($piece_counts * $pattern_counts * $quantity_counts === 0) {
      return false;
    }

    // Verify equality of all request group counts
    if ($piece_counts !== $pattern_counts || $pattern_counts !== $quantity_counts) {
      return false;
    }

    // Verify no empty, null, or zero values passed
    foreach ($input['piece_type_ids'] as $piece_type_id) {
      if (empty($piece_type_id)) {
        return false;
      }
    }

    // Verify no empty, null, or zero values passed
    foreach ($input['pattern_ids'] as $pattern_id) {
      if (empty($pattern_id)) {
        return false;
      }
    }

    // Verify our quantities
    foreach ($input['quantities'] as $quantity) {
      /**
       * This preg-match works "ok" but technically integer 1e4 (1_000) would pass here.
       * A more robust check, with min-max definitions, would be preferable
       */
      if (preg_match('/^\d+$/', $quantity) !== 1) {
        return false;
      }
    }

    return true;
  }

}