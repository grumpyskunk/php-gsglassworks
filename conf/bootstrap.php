<?php

/**
 * This bootstrap file provides a kickoff point for any resource within the gsGlass framework
 *
 * "The purpose of a storyteller is not to tell you how to think, but to give you questions to think upon."
 * - Hoid, The Way of Kings
 */

use gsGlass\api\resources\DependencyInjector;

define('ENV', parse_ini_file(__DIR__ . '/application.ini', INI_SCANNER_RAW));
define('APP_NAME', ENV['application']['name']);
define('APP_PATH', __DIR__ . '/..');
define('CONFIG_PATH', __DIR__);
define('REQUEST_TIMESTAMP', time());

// Define project auto-loader
spl_autoload_register(static function ($class) {
  $namespace = explode('\\', $class);
  $framework = array_shift($namespace);
  $base_name = array_pop($namespace);

  if ($framework === 'gsGlass') {
    // Load from project namespace
    $path = implode('/', $namespace);
    require_once APP_PATH . '/' . $path . '/' . $base_name . '.php';
  }
  // Else framework is vendor product - loading occurs here
});

set_exception_handler(static function ($exception) {
  // Error handling log logic goes here
  echo __LINE__ . 'Uncaught exception: ', $exception, "\n";
});

/**
 * Instantiate dependency injector used to execute API.
 * Wrapper function to aid IDE in method validation
 *
 * @return DependencyInjector
 */
function getDependencyInjector(): DependencyInjector {
  return new DependencyInjector();
}