<?php

namespace gsGlass;

use gsGlass\api\resources\DependencyInjector;
use gsGlass\api\services\PieceRequestService;

/**
 * Initialize application
 */
require_once __DIR__ . '/../conf/bootstrap.php';

/** @var DependencyInjector $dependency_injector */
$dependency_injector = getDependencyInjector();
/** @var PieceRequestService $piece_request_service */
$piece_request_service = $dependency_injector->buildObject(PieceRequestService::class);

$piece_types = $piece_request_service->getAllPieceTypes();
$patterns    = $piece_request_service->getAllPatterns();

?>
<!doctype html>
<html lang="en">
<head>
  <!-- stylesheets -->
  <link rel="stylesheet" type="text/css" href="resources/css/style.css">

  <!-- scripts -->
  <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"></script>

  <title><?php echo APP_NAME; ?></title>

  <!-- Specify character encoding -->
  <meta charset="utf-8">
  <!-- Set viewport to keep screen ratio attached by screen size -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<!-- page header -->
<div id="header">
  <div class="title"><a href="./index.php" title="<?php echo APP_NAME; ?>" ><?php echo APP_NAME; ?></a></div>
  <p class="subtitle">Your #1 provider of unique glassware products</p>
</div>

<!-- page content -->
<div id="main_content" class="container">
  <div id="content">
    <div id="left-copy">
      <h2>Piece Request Form</h2>
      <p>
        At <?php echo APP_NAME; ?> we believe that the perfect tabletop is only possible with the the perfect glassware.
      </p>
      <p>
        That is why we offer you the prompt notification of currently out-of-stock pieces when they
        become available to order. Use the form below to subscribe yourself for immediate notification
        the next time that rare piece of glassware is available for purchase.
      </p>
    </div>
    <div id="right-form">

      <!-- Input form -->
      <form name="piece_request" id="piece_request" method="POST" action="./submit.php" onsubmit="return validate()">

        <div class="form_row">
          <label for="name" class="required">Name: </label>
          <input type="text" name="name" id="name" class="required" size="30">
        </div>

        <div class="form_row">
          <label for="email" class="required">Email: </label>
          <input type="text" name="email" id="email" class="required" size="30">
        </div>

        <div class="form_row">
          <label for="phone">Phone: </label>
          <input type="text" name="phone" id="phone" size="10">
        </div>

        <!-- Request piece detail group -->
        <div id="rq_1" class="request_group">

          <div class="form_row">
            <label id="lbl_piece_id_1" for="piece_id_1" class="required">Type: </label>
            <select name="piece_type_ids[]" id="piece_id_1" class="required">
              <option value="" selected>Select a piece type</option>
              <?php
              foreach ($piece_types as $piece_type) {
                echo sprintf('<option value="%d">%s</option>', $piece_type['id'], $piece_type['type']);
              }
              ?>
            </select>
          </div>

          <div class="form_row">
            <label id="lbl_pattern_id_1" for="pattern_id_1" class="required">Pattern: </label>
            <select name="pattern_ids[]" id="pattern_id_1" class="required">
              <option value="" selected>Select a pattern type</option>
              <?php
              foreach ($patterns as $pattern) {
                echo sprintf('<option value="%d">%s</option>', $pattern['id'], $pattern['pattern']);
              }
              ?>
            </select>
          </div>

          <div class="form_row">
            <label id="lbl_quantity_1" for="quantity_1" class="required">Quantity: </label>
            <input type="text" name="quantities[]" id="quantity_1" size="5">
          </div>
        </div>

        <div class="form_row">
          <button id="addition" type="button" onclick="addRequest()">Additional Request</button>
        </div>

        <div class="form_row">
          * Required fields<br>
          <button id="submit" type="submit">Submit</button>
          <button id="reset" type="reset">Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- footer -->
<footer>
  <small>
    Created by: J. Neiderhiser
  </small>
</footer>

<script>
  // We always start with at least one request and we have a limit of 3 per form
  var requestCount = 1;
  var requestLimit = 3;

  /**
   * Adds an additional request piece group to our input form
   */
  function addRequest() {
    // Get the current count
    var currentCount = requestCount;
    // Iterate our current count for additions
    requestCount += 1;

    // Retrieve and clone from our latest source
    var source = $('#rq_' + currentCount),
      clone = source.clone();


    // Set our clone group IDs
    clone.prop('id', 'rq_' + requestCount);

    clone.find('#lbl_piece_id_' + currentCount).prop('id', 'lbl_piece_id_' + requestCount)
      .prop('for', 'piece_id_' + requestCount);
    clone.find('#piece_id_' + currentCount).prop('id', 'piece_id_' + requestCount);

    clone.find('#lbl_pattern_id_' + currentCount).prop('id', 'lbl_pattern_id_' + requestCount)
      .prop('for', 'pattern_id_' + requestCount);
    clone.find('#pattern_id_' + currentCount).prop('id', 'pattern_id_' + requestCount);

    clone.find('#lbl_quantity_' + currentCount).prop('id', 'lbl_quantity_' + requestCount)
      .prop('for', 'quantity_' + requestCount);
    clone.find('#quantity_' + currentCount).prop('id', 'quantity_' + requestCount);

    // Reset quantity text input
    clone.find('input').val("");

    // And insert new request group
    clone.insertAfter(source);

    // If we've reached our request limit..
    if (requestCount === requestLimit) {
      $('#addition').prop('disabled', true).hide();
    }
  }

  /**
   * Performs javascript based input validation
   *
   * @returns {boolean}
   */
  function validate() {
    var passed = true;

    // Target fields to validate
    var name = $('#name');
    var email = $('#email');

    // Validate name input
    if (name.val() < 1) {
      // Add highlight
      name.addClass('warning');

      // If this is the first failure
      if (passed) {
        name.focus();
      }

      passed = false;
    } else {
      name.removeClass('warning');
    }

    // Validate email input
    if (email.val() < 5) {
      // Add highlight
      email.addClass('warning');

      // If this is the first failure
      if (passed) {
        email.focus();
      }

      passed = false;
    } else {
      email.removeClass('warning');
    }

    // Validate piece type inputs
    $('select[name^="piece_type"').each(
      function () {
        var elem = $('#' + this.id);
        if (elem.val() === "") {
          // If this is the first failure
          if (passed) {
            elem.focus();
          }

          passed = false;
        }
      }
    );

    // Validate pattern inputs
    $('select[name^="pattern"').each(
      function () {
        var elem = $('#' + this.id);
        if (elem.val() === "") {
          // If this is the first failure
          if (passed) {
            elem.focus();
          }

          passed = false;
        }
      }
    );

    // Validate quantity inputs
    $('input[name^="quantities"').each(
      function () {
        var elem = $('#' + this.id);
        if (elem.val() === "" || !elem.val().match(/^\d+/)) {

          // Add highlight
          elem.addClass('warning');

          // If this is the first failure
          if (passed) {
            elem.focus();
          }

          passed = false;
        } else {
          elem.removeClass('warning');
        }
      }
    );

    return passed;
  }
</script>

</body>
</html>
