<?php

namespace gsGlass;

use gsGlass\api\resources\DependencyInjector;
use gsGlass\api\services\PieceRequestService;

/**
 * Initialize application
 */
require_once __DIR__ . '/../conf/bootstrap.php';

/** @var DependencyInjector $dependency_injector */
$dependency_injector = getDependencyInjector();
/** @var PieceRequestService $piece_request_service */
$piece_request_service = $dependency_injector->buildObject(PieceRequestService::class);

// Retrieve form input
$input = $_POST;

?>
<!doctype html>
<html lang="en">
<head>
  <link rel="stylesheet" type="text/css" href="resources/css/style.css">

  <title><?php echo APP_NAME; ?></title>

  <!-- Specify character encoding -->
  <meta charset="utf-8">
  <!-- Set viewport to keep screen ratio attached by screen size -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<!-- page header -->
<div id="header">
  <div class="title"><?php echo APP_NAME; ?></div>
  <p class="subtitle">Your #1 provider of unique glassware products</p>
</div>

<!-- page content -->
<div id="main_content" class="container">
  <div id="content" class="text-centered">
    <?php

    // If we have valid user input...
    if ($piece_request_service->validateRequest($input)) {
      // Place request
      $piece_request_service->placeRequest($input);
      ?>

      <h2>Thank You</h2>
      <p>
        Your piece request has been placed!
      </p>
      <p>
        A representative with <?php echo APP_NAME; ?> will contact you the next time the piece and pattern
        combinations you've requested become available for order. In the meantime if you have any questions
        feel free to contact us at: 800-555-1337.
      </p>

      <?php
    } else {
      // Redirect back to index - someone has been tinkering with the HTML
      ?>
      <meta http-equiv="refresh" content="0; URL=./index.php"/>
      <?php
    }
    ?>
  </div>
</div>

<!-- footer -->
<footer>
  <small>
    Created by: J. Neiderhiser
  </small>
</footer>

</body>
</html>
